class Student {
    constructor(name, age, date, gender, id, hobbies) {
        this.name = name;
        this.age = age;
        this.date = date;
        this.gender = gender;
        this.id = id;
        this.hobbies = hobbies;
    }

    // Set Name
    setName(nama) {
        this.name = nama;
    }
    setAge(umur) {
        this.age = umur;
    }
    setdateOfBirth(dob) {
        this.date = dob;
    }
    setGender(kelamin) {
        if (gender === 'Male' || gender === 'Female') {
            this.gender = kelamin;
        }
    }
    setdateOfBirth(idSiswa) {
        this.id = idSiswa;
    }

    // Add
    setHobbies(hob) {
        this.hobbies.push(hob)
    }
    // Delete
    setRemove = (hobi) => {
        let i = this.hobbies.indexOf(hobi);
        if (i < -1) {
            this.hobbies.splice(i, 1);
        }
    }

    // Buat GetData
    getData = () => {
        return `
        Name : ${this.name} 
        Age : ${this.age} 
        Date : ${this.date} 
        Gender : ${this.gender}
        Id Student : ${this.id} 
        Hobbies : ${this.hobbies}
        `;
    }
}

const user = new Student(
    'Teddy Ferdian Abrar Amrullah',
    22,
    '08/06/1998',
    'Male',
    '0092020017',
    ['Rebahan', 'Makan', 'Gaming']
);


// Pemanggilan
console.log(user.getData());





